export class CommonHttpResponse {
    error: boolean;
    msg: string;
    data?: any;
}
