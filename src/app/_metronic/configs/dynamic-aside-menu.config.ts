export const DynamicAsideMenuConfig = {
  items: [
    {
      title: 'Home PAKAR',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Home/Home.svg',
      page: '/homepage',
      bullet: 'dot',
    },
    {
      title: 'Daskboard',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Shopping/Chart-line1.svg',
      page: '/dashboard',
      bullet: 'dot',
    },
    {
      title: 'Kembali ke myBCAportal',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/coin.svg',
      page: '/builder',
      bullet: 'dot',
    },
    {
      title: 'PAKAR Info',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Code/Info-circle.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
  footer: [
    {
      title: 'PAKAR PDF',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/pdf.svg',
      page: '/info',
      bullet: 'dot',
    },
    {
      title: 'FAQ',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/icons/Code/Question-circle.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
  editor: [
    {
      title: 'Contents',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/content.svg',
      page: '/info',
      bullet: 'dot',
    },
    {
      title: 'My Pages',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/mypage.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
  publisher: [
    {
      title: 'Contents',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/content.svg',
      page: '/info',
      bullet: 'dot',
    },
    {
      title: 'My Pages',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/mypage.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
  admin: [
    {
      title: 'My Pages',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/mypage.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
  super_admin: [
    {
      title: 'Contents',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/content.svg',
      page: '/info',
      bullet: 'dot',
    },
    {
      title: 'My Pages',
      root: true,
      icon: 'flaticon2-architecture-and-city',
      svg: './assets/media/svg/bca/aside/mypage.svg',
      page: '/info',
      bullet: 'dot',
    }
  ],
};
